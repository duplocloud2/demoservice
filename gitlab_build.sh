#!/bin/bash -e
docker info
docker login registry.gitlab.com/duplocloud/demoservice -u $DOCKER_USER -p $DOCKER_PASS
docker build -t registry.gitlab.com/duplocloud/demoservice:$CI_COMMIT_SHA .
docker push registry.gitlab.com/duplocloud/demoservice:$CI_COMMIT_SHA

echo "Docker Build Finished.."
